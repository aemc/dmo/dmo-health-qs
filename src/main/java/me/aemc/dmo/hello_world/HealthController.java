package me.aemc.dmo.hello_world;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

class HealthStatus
{
  public boolean status;
  public void setStatus( boolean status ){ this.status = status; }
}

@Path("/health")
public class HealthController
{
  final Logger log = LoggerFactory.getLogger( this.getClass().getName() );
  boolean upAndRunning = true;
  
  @GET
  @Produces( MediaType.TEXT_PLAIN)
  public Response handleGetRequest(){
    this.log.info( "b: handleGetRequest()" );
  
    String retVal = "UP";
    
    Response rsp = null;
    
    if( !this.upAndRunning ) {
      retVal = "DOWN";
      rsp = Response.status( 503, "Service unavailable" ).entity(retVal).build();
    } else {
      rsp = Response.ok(retVal).build();
    }
  
    this.log.info( "e: handleGetRequest() - isUpAndRunning: {}", this.upAndRunning );
    return rsp;
  }
  
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  public Response handlePutRequest( HealthStatus body )
  {
    this.log.info( "b: handlePutRequest( {} )", body.status );
    
    this.upAndRunning = body.status;
    
    this.log.info( "e: handlePutRequest() - isUpAndRunning: {}", this.upAndRunning );
    return Response.ok(this.upAndRunning).build();
  }
}
