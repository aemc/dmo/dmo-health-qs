= README - evl-quarkus01

----
mvn io.quarkus:quarkus-maven-plugin:1.12.2.Final:create \
    -DprojectGroupId=me.aemc.dmo \
    -DprojectArtifactId=dmo-health-qs \
    -DclassName="me.aemc.dmo.health.App" \
    -Dpath="/hello"

mvn quarkus:list-extensions

mvn compile quarkus:dev

mvn package
docker build -f src/main/docker/Dockerfile.jvm -t aemc/dmo-health-qs:latest .
docker run -i --rm -p 8080:8080 aemc/dmo-health-qs:latest

docker image tag aemc/dmo-health-qs:latest \
  registry.gitlab.com/aemc/dmo/dmo-health-qs:20210321T1530
docker image push registry.gitlab.com/aemc/dmo/dmo-health-qs:20210321T1530
----

You can create custom profile names by enabling the profile either setting quarkus.profile system property or QUARKUS_PROFILE environment variable.

----
./mvnw compile quarkus:dev -Dquarkus.profile=local
----

By placing an application.properties file inside a directory named config which resides in the directory where the application runs, any runtime properties defined in that file will override the default configuration. Furthermore any runtime properties added to this file that were not part of the original application.properties file will also be taken into account. This works in the same way for runner jar and the native executable.

.doesn't work :-(
----
./mvnw compile quarkus:dev -Dsmallrye.config.locations=/home/bf/Documents/Projects/local/evl-quarkus/evl-quarkus-01/src/configs/application-v1.properties

$ cd target/quarkus-app
$ java -jar ./quarkus-run.jar -Dsmallrye.config.locations=/home/bf/Documents/Projects/local/evl-quarkus/evl-quarkus-01/src/configs/application-v1.properties
----

== Functionality

=== HelloWorld

----
$ curlie -w '\n' http://localhost:8080/
HTTP/1.1 200 OK
Content-Length: 25
Content-Type: application/json

{
    "text": "Hello World"
}
----

=== Host Info

----
$ curlie -w '\n' http://localhost:8080/info-host
HTTP/1.1 200 OK
Content-Length: 42
Content-Type: application/json

{
    "hostname": "tux",
    "ipAddress": "127.0.1.1"
}
----

=== Message

* returns configured message
* test for dynamically changed configuration in k8s via ConfigMap

----
$ curlie -w '\n' http://localhost:8080/message
HTTP/1.1 200 OK
Content-Length: 9
Content-Type: text/plain;charset=UTF-8

hello dev
----

=== Healthiness-Probe

----
$ curlie -w '\n' http://localhost:8080/health
HTTP/1.1 200 OK
Content-Length: 2
Content-Type: text/plain;charset=UTF-8

UP

$ curlie -w '\n' http://localhost:8080/health \
  -X PUT -H 'Content-Type: application/json' \
  -d '{"status": "false"}'
HTTP/1.1 200 OK
Content-Length: 5
Content-Type: text/plain;charset=UTF-8

false

$ curlie -w '\n' http://localhost:8080/health
HTTP/1.1 503 Service Unavailable
Content-Length: 4
Content-Type: text/plain;charset=UTF-8

DOWN
----
