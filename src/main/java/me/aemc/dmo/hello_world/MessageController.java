package me.aemc.dmo.hello_world;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/message")
public class MessageController
{
  @ConfigProperty( name="message" )
  String message;
  
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String handleGet()
  {
    return this.message;
  }
}
